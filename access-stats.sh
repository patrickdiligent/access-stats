#!/bin/sh

export JAVA_HOME=/Library/Java/JavaVirtualMachines/jdk1.8.0_162.jdk/Contents/Home

JAVA_EXEC=$JAVA_HOME/bin/java

$JAVA_EXEC -classpath ./groovy-all-2.4.12.jar:./commons-cli-1.2.jar:./rxjava-1.3.4.jar:./groovy-json-3.0.0-alpha-1.jar:./h2-1.4.196.jar:./access-stats.jar AccessStatsOp $*

